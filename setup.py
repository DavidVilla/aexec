#!/usr/bin/python

import os
from distutils.core import setup

version = file('debian/changelog').readline().split()[1][1:-1]

setup(name         = 'aexec',
      version      = version,
      description  = 'aexec executes the given program when files change',
      author       = 'David Villa Alises',
      author_email = 'david.villa@uclm.es>',
      url          = 'https://arco.esi.uclm.es:3000/projects/auto-exec',
      license      = 'GPL v3 or later',
      scripts      = ['aexec.py'],
      data_files   = [('/usr/share/pixmaps',
                       [os.path.join('images', x) for x in os.listdir('images')])],
      )
