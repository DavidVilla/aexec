#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

files=$(LANG=C make -nd 2> /dev/null |\
        grep Considering |\
         awk '{print substr($4, 2, length($4)-3)}' )

cmd="/usr/bin/aexec --notify-events"

for i in $files; do
	if [ -e $i ]; then
		cmd="$cmd -w $i"
	fi
done

if grep "clean:" Makefile; then
	make clean
fi

cmd="$cmd make $*"
$cmd
