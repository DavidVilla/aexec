0.20120418
==========

* some unicode conversion issues fixed

0.20120228
==========

* new option to notify file events.

0.20120205
==========

* Handle icon loading failures to deactivate desktop notifications.
* Block SIGINT signal while pyinotify is loading
* Allows to specify targets for amake

0.20111221
==========

* Migrating to gi.repository.Notify (gnome-shell compliant notifications)


0.20110831
==========

* close() notifications


0.20110818
==========

* deal with a Debian notification-daemon bug (#636435).


0.20110601
==========

* ``amake`` skips non existing files.


0.20110529
==========

* New command ``amake``. It runs ``aexec make`` watching Makefile dependencies.






.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
