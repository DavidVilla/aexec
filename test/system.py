#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import os
from unittest import TestCase

import aexec


class TestLoadMakefileDepends(TestCase):
    def setUp(self):
        self.base = os.getcwd()
        os.chdir('test/make')

    def tearDown(self):
        os.chdir(self.base)

    def test_get_files(self):
        files = aexec.get_makefile_depends('Makefile.sample')
        self.assertItemsEqual(files, ['Makefile.sample', 'output', 'source'])

    def test_build_amake_command(self):
        command = aexec.build_amake_command('Makefile.sample')
        self.assertEquals(
            command,
            '/usr/bin/aexec -e -w Makefile.sample -w source')
