#!/usr/bin/python3 -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import signal
sigint_handler = signal.getsignal(signal.SIGINT)
signal.signal(signal.SIGINT, signal.SIG_IGN)

import os
import sys
import time
import subprocess as sp
import logging
import argparse

import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify, GLib, GdkPixbuf
from gi.repository.GLib import GError
import pyinotify

import re
# http://stackoverflow.com/questions/14693701/how-can-i-remove-the-ansi-escape-sequences-from-a-string-in-python
ansi_escape = re.compile(r'\x1b[^m]*m')

# from loggingx import CapitalLoggingFormatter

signal.signal(signal.SIGINT, sigint_handler)

# from pyarco.Conio
ESC = chr(27)
CLS = ESC + '[2J' + ESC + '[0;0f'

IMAGEDIR = os.path.join(os.path.abspath(os.path.split(__file__)[0]), 'images')
if not os.path.exists(IMAGEDIR):
    IMAGEDIR = '/usr/share/pixmaps'


def load_icons():
    def pixbuf(fname):
        return GdkPixbuf.Pixbuf.new_from_file(os.path.join(IMAGEDIR, fname))

    if "DISPLAY" not in os.environ:
        return dict()

    try:
        return {logging.ERROR: pixbuf('red.svg'),
                logging.INFO:  pixbuf('green.svg'),
                logging.DEBUG: pixbuf('orange.svg')}
    except GError:
        return dict()


ICONS = load_icons()


class CommandNotFound(Exception):
    pass


def get_makefile_depends(makefile):
    out = check_output('make -nd -f %s | grep Considering' % makefile,
                         env={'LANG': 'C'}, shell=True)
    lines = out.split('\n')

    retval = []
    for line in lines:
        line = line.strip()
        if not line:
            continue

        retval.append(line.split()[-1][1:-2])

    return retval


def build_amake_command(makefile):
    retval = '/usr/bin/aexec -e'
    files = get_makefile_depends(makefile)

    for f in files:
        if os.path.exists(f):
            retval += ' -w %s' % f

    return retval

#def check_output2(*popenargs, **kargs):
#    kargs['shell'] = True
#    kargs['stdout'] = sp.PIPE
#    process = sp.Popen(*popenargs, **kargs)
#    retcode = process.wait()
#    if retconde != 0:
#        raise sp.CalledProcessError(retcode)
#    return process.stdout.read()


# #backport from Python-2.7
# class CalledProcessError(Exception):
#     """This exception is raised when a process run by check_call() or
#     check_output() returns a non-zero exit status.
#     The exit status will be stored in the returncode attribute;
#     check_output() will also store the output in the output attribute.
#     """
#     def __init__(self, returncode, cmd, output=None):
#         self.returncode = returncode
#         self.cmd = cmd
#         self.output = output
#
#     def __str__(self):
#         return "Command '%s' returned non-zero exit status %d" % (self.cmd, self.returncode)


#backport from Python-2.7 (modified)
def check_output(*popenargs, **kwargs):
    r"""Run command with arguments and return its output as a byte string.

    If the exit code was non-zero it raises a CalledProcessError.  The
    CalledProcessError object will have the return code in the returncode
    attribute and output in the output attribute.

    The arguments are the same as for the Popen constructor.  Example:

    >>> check_output(["ls", "-l", "/dev/null"])
    'crw-rw-rw- 1 root root 1, 3 Oct 18  2007 /dev/null\n'

    The stdout argument is not allowed as it is used internally.
    To capture standard error in the result, use stderr=STDOUT.

    >>> check_output(["/bin/sh", "-c",
    ...               "ls -l non_existent_file ; exit 0"],
    ...              stderr=STDOUT)
    'ls: non_existent_file: No such file or directory\n'
    """
    if 'stdout' in kwargs:
        raise ValueError('stdout argument not allowed, it will be overridden.')
    process = sp.Popen(stdout=sp.PIPE, stderr=sp.PIPE, *popenargs, **kwargs)
    output, err = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        raise sp.CalledProcessError(retcode, cmd, output=err)
    return output


class GI_NotifyHandler(logging.Handler):
    notification = None

    def __init__(self, *args, **kargs):
        logging.Handler.__init__(self, *args, **kargs)
        Notify.init(config.command_line)

    def wait_previous(self):
        if self.notification is None:
            return

        elapsed = time.time() - self.notification.time
        remain = self.notification.timeout - elapsed
#        print self.notification.timeout, elapsed, remain
        if remain > 0:
            time.sleep(0.6 + remain)

        try:
            self.notification.close()
        except GLib.GError as e:
            config.log.error('dbus error: %s', e)

    def emit(self, record):
        self.wait_previous()

        message = record.getMessage().strip()
        try:
            summary, details = message.split('\n', 1)
        except ValueError:
            summary, details = message, ''

        self.timeout = 1000 + 300.0 * (details.count('\n'))

        if details:
            last_lines = [x for x in details.split('\n')[-11:] if x.strip()]
            details = '::\n' + str.join('\n', last_lines)

        self.create_notification(summary, details, ICONS[record.levelno])

    def create_notification(self, summary, details, icon):
        details = ansi_escape.sub('', details)

        self.notification = Notify.Notification.new(summary, details, None)
        self.notification.set_image_from_pixbuf(icon)
        self.notification.set_hint("transient", GLib.Variant.new_boolean(True))
        self.notification.set_urgency(urgency=Notify.Urgency.NORMAL)
        self.notification.set_timeout(self.timeout)

        self.notification.time = time.time()
        self.notification.timeout = self.timeout / 1000

        try:
            self.notification.show()
        except GLib.GError as e:
            config.log.error('notification-daemon connect error: %s', e)


class Command:
    def __init__(self, cmdline, cwd):
        self.cmdline = cmdline
        self.cwd = cwd
        self.reset()

    def reset(self):
        self.returncode = None
        self.out = None
        self.err = None

    def run(self):
        self.reset()
        try:
            config.log.info("running '%s'", self.cmdline)
            self.out = check_output(self.cmdline, cwd=self.cwd, shell=True)
            self.returncode = 0

        except sp.CalledProcessError as e:
            self.err = e.output
            self.returncode = e.returncode

            if e.returncode == 127:
                raise CommandNotFound(self.cmdline)

            raise

    def __repr__(self):
        return self.cmdline


def get_string(obj, encoding='utf-8'):
    assert isinstance(obj, (str, bytes)), type(obj)
#    if not isinstance(obj, (unicode, str)):
#        return str(obj)

    if isinstance(obj, bytes):
        return obj.decode(encoding, 'replace')

    return obj


def run_with_notification(command):
    def format(command, output):
        return "[{0}]$ {1}\n{2}".format(
            command.returncode,
            command,
            get_string(output))

    try:
        command.run()
        config.out.info(format(command, command.out))
    except sp.CalledProcessError:
        config.out.error(format(command, command.err))


class ModifyHandler(pyinotify.ProcessEvent):
    def __init__(self):
        self.event = None

    def process_IN_MODIFY(self, event):
        config.log.debug(event)
        self.event = event

    def test_and_clear(self):
        retval = self.event
        self.event = None
        return retval


class AutoExecutor:
    def __init__(self, command, path):
        self.command = command

        wm = pyinotify.WatchManager()
        for i in path:
            wm.add_watch(i, pyinotify.ALL_EVENTS, rec=True, auto_add=True)

        self.handler = ModifyHandler()
        self.notifier = pyinotify.Notifier(wm, timeout=10,
                                           default_proc_fun=self.handler)
        config.log.info("watching '%s' (C-c to quit)", path)

    def check_events(self):
        self.notifier.process_events()
        retval = self.notifier.check_events()
        if retval:
            self.notifier.read_events()
            self.notifier.process_events()

        return retval

    def run(self):
        tinit = time.time()

        while 1:
            while self.check_events():
                pass

            event = self.handler.test_and_clear()
            if event is None:
                continue

            if '_flymake' in event.pathname:
                continue

            if config.notify_events:
                config.out.debug('file event: %s', event.pathname)

            if time.time() - tinit < config.delta:
                continue

            tinit = time.time()
            check_output('sync')
            run_with_notification(self.command)

            self.check_events()
            self.handler.test_and_clear()


def X_display_available():
    return "DISPLAY" in os.environ and ICONS


def get_config():
    parser = argparse.ArgumentParser(description='Exec a command when files change.')
    parser.add_argument('-a', '--avoid-notify', action='store_true',
                        help='do not use desktop notifier')
    parser.add_argument('-d', '--delta', default=0.5, type=float,
                        help='mininum considered time between events (seconds)')
    parser.add_argument('-e', '--notify-events', action='store_true',
                        help='notify file events')
    parser.add_argument('-t', '--timetag', action='store_true',
                        help='include time info in the logs')
    parser.add_argument('-v', dest='verbosity', default=0, action='count',
                        help='verbosity level')
    parser.add_argument('-w', '-watch', dest='path', metavar="PATH",
                        action='append', default=[],
                        help='watch file or directory')
    parser.add_argument('--cwd', default=os.getcwd(),
                        help='change current directory')
    parser.add_argument(dest='command_line', nargs='+',
                        help='the command to execute')

    retval = parser.parse_args()

    retval.command_line = str.join(' ', retval.command_line)

    if not X_display_available():
        print("warning: desktop notification not available")
        retval.avoid_notify = True

    os.chdir(retval.cwd)

    return retval


def setup_loggers(config):
    config.log = logging.getLogger('log')
    config.out = logging.getLogger('out')

    timetag = '%(asctime)s ' if config.timetag else ''
    # formatter = CapitalLoggingFormatter(
    #     '{0}[%(levelcapital)s] %(name)s: %(message)s'.format(timetag),
    #     '%d/%m/%y %H:%M:%S')

    console = logging.StreamHandler()
    # console.setFormatter(formatter)

    config.log.addHandler(console)
    config.log.setLevel(max(logging.DEBUG, logging.ERROR - config.verbosity * 10))
    config.log.info("vesbosity is %s", config.verbosity)

    config.out.addHandler(console)
    config.out.setLevel(logging.DEBUG)

    if not config.avoid_notify:
        config.out.addHandler(GI_NotifyHandler())


def main():
    command = Command(config.command_line, config.cwd)
    try:
        run_with_notification(command)

    except CommandNotFound as e:
        msg = get_string(e.message)
        print(type(msg))
        config.out.error("Command not found\n%s", msg)
        return 1

    try:
        AutoExecutor(command, config.path).run()
    except KeyboardInterrupt:
        print('(cancel)')
        return 0


if __name__ == '__main__':
    config = get_config()
    setup_loggers(config)
    sys.exit(main())
